# Building apps

## Managing multiple projects using Visual Studio 2022

solution concept - allows you to open and manage multiple projects simultaneously.

## Understanding the compiler-generated folders and files

Two compiler-generated folders were created, named obj and bin. Compiler needs to create temporary folders and files to do its work. You could delete these folders and their files, and they can be recreated later. Developers often do this to "clean" a project. 

- obj - this folder contains one compiled object file for each source code file. These objects haven't been linked together into a final executable yet.  
- bin - this folder contains the binary executable for the application or class library.

Boilerplate for creating console programs:

```cs
using System;

namespace HelloCS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
```

The **top-level program** boilerplate possible as of C# 9:

```cs
using System;
Console.WriteLine("Hello World!");
```

Importing files with namespaces are automatically created by the compiler for projects that target .NET 6, and that it uses a feature introduced in C# 10 called **global imports** that imports some commonly used namespaces like System for use in all code files.

## Building console apps using Visual Studio Code

Visual Studio Code has a concept named a workspace that allows you to open and manage
multiple projects simultaneously.  
Navigate to **File | Save Workspace As...** -> create new folder -> create new folder for workspace -> save the workspace -> Navigate to **File | Add Folder to Workspace...** or click the **Add Folder** button. -> in workspace folder create project folder -> in project folder click **Add** button -> navigate to **View | Terminal** -> in terminal run command: `dotnet new console -f net5.0`

New created project contains:
- `HelloCS.csproj` file
- `Program.cs` file
- `obj` folder

To compile and run the code enter following command in console:
```sh
dotnet run
```

Adding a second project using Visual Studio Code:
- navigate to **File | Add Folder to Workspace...**
- create new folder for projet and click **Add**
- open new terminal nad select **TopLevelProgram**
- in new folder run: `dotnet new console`
- change project using **OmniSharp: Select Project**
- run: `dotnet run`

# Sources

- [.NET Blog](https://devblogs.microsoft.com/dotnet/)  
- [Watching Scott Hanselman's videos](https://www.youtube.com/c/shanselman)