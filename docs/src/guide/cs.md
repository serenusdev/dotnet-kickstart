# C# programming language

## Statement

Instrukcja jest jak kompozycja poszczególnych składników według odpowiednich reguł.

Instrukcja może się składać z wielu zmiennych i wyrażeń (expressions).

## Comments

// - double slash
/* */ - multiple line comment block

# Blocks

Blocks start with a declaration to indicate what is being defined.

# Importing namespaces

When new project is creating ant it targets .NET 6.0 and therefore use the C# 10, compiler generate a `*.cs` file in the `obj` folder to implicitly globally import some common namespaces.

To see this file go to: `obj/Debug/net6.0/`. There is file named `TopLevelProgram.GlobalUsings.g.cs`.

Global namespaces may be added to or removed in `*.csproj` file:

```xml{10-13}
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net6.0</TargetFramework>
    <Nullable>enable</Nullable>
    <ImplicitUsings>enable</ImplicitUsings>
  </PropertyGroup>

  <ItemGroup>
    <Using Remove="System.Threading" />
    <Using Include="System.Numerics" />
  </ItemGroup>
  
</Project>
```

Note `GlobalUsings` file now imports System.Numerics instead of System.Threading.


You can disable the implicitly imported namespace feature for all SDKs by removing an entry in the project file:

```xml
<ImplicitUsings>enable</ImplicitUsings>
```

Verbs are methods.  
In C#, doing or action words are called **methods**.   
Nouns are **types**, **variables**, **fields**, and **properties**.
Nouns are **
types**, **variables**, **fields**, and **properties**.


**literal** value is a notation that represents a fixed value.

**verbatim literal string** - prefix literal string by `@`. Then escaped chars will not be escaped by backslash.

**Interpolated string** - prefixed by `$` to enable embedded formatted variables.

string