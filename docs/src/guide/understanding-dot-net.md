# Understanding .NET

.NET 6, .NET Core, .NET Framework, and Xamarin are related and overlapping platforms for developers used to build applications and services.

## Understanding .NET Framework

It is a development platform that includes a 
- **Common Language Runtime (CLR)**, which manages execution of code and a 
- **Base Class Library (BCL)**, which provides a rich library of classes to build applications from.

**Global Assembly Cache (GAC)** - CLR libraries.

.NET Framework is Windows-only and a legacy platform. Don't create new apps using it.

## Understanding the Mono, Xamarin, and Unity projects

- **Mono** - developed by third parties .NET Framework inplementation. It is cross-platform. Mono has found a niche as the foundation of the **Xamarin** mobile platform as well as cross-platform game development platforms like **Unity**.

## Understanding .NET Core

While rewriting .NET Framework to be truly cross-platform, they've taken the opportunity to refactor and remove major parts that are no longer considered core.

.NET Core includes a cross-platform implementation of the
CLR known as **CoreCLR** and a streamlined BCL known as **CoreFX**.

.NET Core 3.1 included **Blazor Server** for building web components. Microsoft had
also planned to include Blazor WebAssembly in that release, but it was delayed. Blazor
WebAssembly was later released as an optional add-on for .NET Core 3.1.

**.NET Runtime**  
**.NET SDK**  

Modern .NET is modularized compared to the legacy .NET Framework, which is monolithic. It is open source and improved in performance. It is smaller because of removal of legacy and non-cross- platform technologies.

Workloads such as **Windows Forms** and **Windows
Presentation Foundation (WPF)** can be used to build **graphical user interface (GUI)** applications, but they are tightly bound to the Windows ecosystem, so they are not included with .NET on macOS and Linux.

## Web development

ASP.NET Web Forms and Windows Communication Foundation (WCF) are old web
application and service technologies that fewer developers are choosing to use for new
development projects today, so they have also been removed from modern .NET. Instead,
developers prefer to use ASP.NET MVC, ASP.NET Web API, SignalR, and gRPC. These
technologies have been refactored and combined into a platform that runs on modern .NET, named ASP.NET Core. 

## Database development

**Entity Framework (EF) 6** is an object-relational mapping technology that is designed to work
with data that is stored in relational databases such as Oracle and Microsoft SQL Server. It has
gained baggage over the years, so the cross-platform API has been slimmed down, has been
given support for non-relational databases like Microsoft Azure Cosmos DB, and has been
renamed Entity Framework Core. 

## Themes of modern .NET

https://themesof.net

## Understanding .NET Standard

The situation with .NET in 2019 was that there were three forked .NET platforms controlled by
Microsoft, as shown in the following list:
- .NET Core: For cross-platform and new apps
- .NET Framework: For legacy apps
- Xamarin: For mobile apps

Because of that, Microsoft defined .NET Standard – a specification for a set of APIs that all .NET
platforms could implement to indicate what level of compatibility they have. For example,
basic support is indicated by a platform being compliant with .NET Standard 1.4.

With .NET Standard 2.0 and later, Microsoft made all three platforms converge on a modern
minimum standard, which made it much easier for developers to share code between any
flavor of .NET.

For .NET Core 2.0 and later, this added most of the missing APIs that developers need to
port old code written for .NET Framework to the cross-platform .NET Core. 

To use .NET Standard, you must install a .NET platform that implements the .NET Standard
specification. The last .NET Standard, version 2.1, is implemented by .NET Core 3.0, Mono,
and Xamarin. Some features of C# 8.0 require .NET Standard 2.1. .NET Standard 2.1 is not
implemented by .NET Framework 4.8, so we should treat .NET Framework as legacy.

With the release of .NET 6 in November 2021, the need for .NET Standard has reduced
significantly because there is now a single .NET for all platforms, including mobile. .NET 6 has
a single BCL and two CLRs: CoreCLR is optimized for server or desktop scenarios like websites
and Windows desktop apps, and the Mono runtime is optimized for mobile and web browser
apps that have limited resources.
Even now, apps and websites created for .NET Framework will need to be supported, so it is
important to understand that you can create .NET Standard 2.0 class libraries that are backward
compatible with legacy .NET platforms.

**.NET MAUI (Multi-platform App UI).** - Building Mobile and Desktop Apps.

## Understanding intermediate language

The C# compiler (named **Roslyn**) used by the dotnet CLI tool converts your C# source code
into **intermediate language (IL)** code and stores the IL in an **assembly** (a DLL or EXE file). IL
code statements are like assembly language instructions, which are executed by .NET's virtual
machine, known as CoreCLR.

At runtime, CoreCLR loads the IL code from the assembly, **the just-in-time (JIT)** compiler
compiles it into native CPU instructions, and then it is executed by the CPU on your machine.

Microsoft and others provide disassembler tools that can open an assembly and reveal this IL code, such as the ILSpy .NET Decompiler extension.

