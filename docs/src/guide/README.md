# Introduction

Let term **modern .NET** refer to .NET 6 and its predecessors like
.NET 5 that come from .NET Core.   
Let term **legacy .NET** refer to .NET Framework,
Mono, Xamarin, and .NET Standard. Modern .NET is a unification of those legacy platforms
and standards.  

Understanding
the similarities and differences between 
- modern .NET, 
- .NET Core, 
- .NET Framework, 
- Mono,
- Xamarin
- .NET Standard

## Dev Tools

- Visual Studio Code
- Visual Studio 2022 for C# 10 and .NET 6
- JetBrains Rider
- GitHub Codespaces for development in the cloud
- .NET Interactive Notebooks extension


Visual Studio Code is the most modern and lightweight code editor for cross-platform development.

## Set up .NET on Ubuntu

Download page for .NET 6.0 LTS version: https://dotnet.microsoft.com/en-us/download  
[Instalowanie programu .NET w systemie Linux](https://docs.microsoft.com/pl-pl/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website)  
[Instalowanie zestawu SDK platformy .NET lub środowiska uruchomieniowego platformy .NET w systemie Ubuntu](https://docs.microsoft.com/pl-pl/dotnet/core/install/linux-ubuntu)

- Ubuntu 21.10
- .NET Core 3.1
- .NET 5
- .NET 6

Należy zainstalować zestaw SDK, który zawiera środowisko uruchomieniowe.  
Środowisko uruchomieniowe służy jedynie do uruchamiania aplikacji.  
Zalecana jest instalacja środowiska uruchomieniowego programu ASP.NET Core. Zawiera ono zarówno środowisko uruchomieniowe .NET, jak i ASP.NET Core.

Aby sprawdzić wersje zestawu należy w konsoli użyć poleceń:
- `dotnet --list-sdks`
- `dotnet --list-runtimes`

### Installation

> Pakiety .NET 6 nie zostały jeszcze opublikowane dla systemu Ubuntu 21.10. Następujące polecenie `wget` używa repozytorium Ubuntu 21.04. Ten artykuł zostanie zaktualizowany, gdy pakiety będą dostępne w repozytorium Ubuntu 21.10.

```shell
wget https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
```

#### Instalacja zestawu SDK

```sh
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-6.0
```

#### Instalacja środowiska uruchomieniowego

```sh
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y aspnetcore-runtime-6.0
```

#### Instalacja środowiska uruchomieniowego bez ASP.NET Core Runtime

```sh
sudo apt-get install -y dotnet-runtime-6.0
```

```sh
dotnet --info
# Zestaw .NET SDK (odzwierciedlenie dowolnego pliku global.json):
#  Version:   6.0.201
#  Commit:    ef40e6aa06

# Środowisko uruchomieniowe:
#  OS Name:     ubuntu
#  OS Version:  21.10
#  OS Platform: Linux
#  RID:         ubuntu.21.10-x64
#  Base Path:   /usr/share/dotnet/sdk/6.0.201/

# Host (useful for support):
#   Version: 6.0.3
#   Commit:  c24d9a9c91

# .NET SDKs installed:
#   5.0.406 [/usr/share/dotnet/sdk]
#   6.0.201 [/usr/share/dotnet/sdk]

# .NET runtimes installed:
#   Microsoft.AspNetCore.App 5.0.15 [/usr/share/dotnet/shared/Microsoft.AspNetCore.App]
#   Microsoft.AspNetCore.App 6.0.3 [/usr/share/dotnet/shared/Microsoft.AspNetCore.App]
#   Microsoft.NETCore.App 5.0.15 [/usr/share/dotnet/shared/Microsoft.NETCore.App]
#   Microsoft.NETCore.App 6.0.3 [/usr/share/dotnet/shared/Microsoft.NETCore.App]

# To install additional .NET runtimes or SDKs:
#   https://aka.ms/dotnet-download
```

### MSSQL installation

1. Import the public repository GPG keys:

```sh
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
```

2. Register the Microsoft SQL Server Ubuntu repository for SQL Server 2019:

```sh
sudo add-apt-repository "$(wget -qO- https://packages.microsoft.com/config/ubuntu/20.04/mssql-server-2019.list)"
```

3. Run the following commands to install SQL Server:

```sh
sudo apt-get update
sudo apt-get install -y mssql-server
```

4. After the package installation finishes, run mssql-conf setup and follow the prompts to set the SA password and choose your edition.

```sh
sudo /opt/mssql/bin/mssql-conf setup
```

5. Once the configuration is done, verify that the service is running:

```sh
systemctl status mssql-server --no-pager
```

6. If you plan to connect remotely, you might also need to open the SQL Server TCP port (default 1433) on your firewall.

To create a database, you need to connect with a tool that can run Transact-SQL statements on the SQL Server. The following steps install the SQL Server command-line tools: sqlcmd and bcp.

Install the SQL Server command-line tools:

```sh
sudo apt-get update 
sudo apt install curl
# Import the public repository GPG keys.
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
# Register the Microsoft Ubuntu repository.
curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list
# Update the sources list and run the installation command with the unixODBC developer package
sudo apt-get update 
sudo apt-get install mssql-tools unixodbc-dev
# Optional: Add `/opt/mssql-tools/bin/` to your PATH environment variable in a bash shell.
# To make sqlcmd/bcp accessible from the bash shell for login sessions, modify your PATH in the ~/.bash_profile file with the following command:
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
# To make sqlcmd/bcp accessible from the bash shell for interactive/non-login sessions, modify the PATH in the ~/.bashrc file with the following command:
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc
```

Connect locally

The following steps use sqlcmd to locally connect to your new SQL Server instance.

```sh
# Run sqlcmd with parameters for your SQL Server name (-S), the user name (-U), and the password (-P). In this tutorial, you are connecting locally, so the server name is localhost. The user name is SA and the password is the one you provided for the SA account during setup.
sqlcmd -S localhost -U SA -P '<YourPassword>'
# If successful, you should get to a sqlcmd command prompt: 1>.
```

Create and query data

The following steps create a new database named `TestDB`.

```sh
# From the sqlcmd command prompt, paste the following Transact-SQL command to create a test database:
CREATE DATABASE TestDB
# On the next line, write a query to return the name of all of the databases on your server:
SELECT Name from sys.Databases
# The previous two commands were not executed immediately. You must type GO on a new line to execute the previous commands:
GO
# 
# Insert data
# Next create a new table, Inventory, and insert two new rows.
# From the sqlcmd command prompt, switch context to the new TestDB database:
USE TestDB
# Create new table named Inventory:
CREATE TABLE Inventory (id INT, name NVARCHAR(50), quantity INT)
# Insert data into the new table:
INSERT INTO Inventory VALUES (1, 'banana', 150); INSERT INTO Inventory VALUES (2, 'orange', 154);
# Type GO to execute the previous commands:
GO
#
# Select data
# Now, run a query to return data from the Inventory table.
# From the sqlcmd command prompt, enter a query that returns rows from the Inventory table where the quantity is greater than 152:
SELECT * FROM Inventory WHERE quantity > 152;
GO
#
# To end your sqlcmd session, type QUIT:
QUIT
```

### CS Code extensions

- C# for Visual Studio Code (powered by OmniSharp)
  ms-dotnettools.csharp
- .NET Interactive Notebooks
  ms-dotnettools.dotnet-interactive-vscode
- MSBuild project tools
  tinytoy.msbuild-project-tools
- REST Client
  humao.rest-client
- ILSpy .NET Decompiler
  icsharpcode.ilspy-vscode
- Azure Functions for Visual Studio Code
  ms-azuretools.vscode-azurefunctions
- GitHub Repositories
  github.remotehub
- SQL Server (mssql) for Visual Studio Code
  ms-mssql.mssql
- Protobuf 3 support for Visual Studio Code
  zxh404.vscode-proto3
# Source

- [Using a Raspberry Pi 400 with Ubuntu Desktop 64-bit](https://github.com/markjprice/cs9dotnet5-extras/blob/main/raspberry-pi-ubuntu64/README.md)  
- [Quickstart: Install SQL Server and create a database on Ubuntu](https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-ubuntu?view=sql-server-ver15)

