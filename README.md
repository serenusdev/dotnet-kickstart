# Dotnet Kickstart

- .NET Core
- .NET Framework

- C# lang
- .NET libs
- app models (websites)
- services
- desktop
- mobile apps

# Sources

- Mark J. Price, _C# 10 and .NET 6 – Modern Cross-Platform Development_, ISBN 978-1-80107-736-1, Packt Publishing Ltd., Sixth edition: November 2021